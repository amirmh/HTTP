import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { ConfigServiceComponent } from './config-service/config-service.component';
import { RouterModule, Routes } from '@angular/router';
import { PostComponent } from './config-service/post/post.component';



const routes: Routes = [
  { path: '', redirectTo: '/posts' ,pathMatch: 'full'},
  {path: 'posts',component: ConfigServiceComponent},
  {path: 'posts/:id',component: PostComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    ConfigServiceComponent,
    PostComponent,
  ],
  imports: [
    RouterModule,
    // Routes,
    RouterModule.forRoot(routes),
    BrowserModule,
    HttpClientModule
  ],
  exports: [ RouterModule ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit  } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {LocationStrategy} from '@angular/common';
@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  constructor(private http: HttpClient,private url:LocationStrategy) {
  }

  // IDsULRChild='https://jsonplaceholder.typicode.com/posts/2';
  IDsULRChild='';
  getUser() {
    return this.http.get(this.IDsULRChild);
  }
  UserData={};
  showUserData() {
    var _this=this
    this.getUser()
      .subscribe(function(data:any){
        _this.UserData=data;
        // console.log(_this.UserData);
      });
  }

  ngOnInit() {
    this.IDsULRChild='https://jsonplaceholder.typicode.com'+this.url.path();
    // console.log(this.IDsULRChild);
    this.showUserData();

  }

}

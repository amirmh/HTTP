import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {post} from 'selenium-webdriver/http';
import {PostComponent} from './post/post.component';


@Component({
  selector: 'app-config-service',
  templateUrl: './config-service.component.html',
  styleUrls: ['./config-service.component.css']
})
export class ConfigServiceComponent implements OnInit {

  constructor(private http: HttpClient) { }
  configUrl = 'https://jsonplaceholder.typicode.com/posts';
  @ViewChild('littleTimmy') littleTimmy: PostComponent;
  getTitle() {
    return this.http.get(this.configUrl);
  }
  PostData=[];
  showConfig() {
    var _this=this
    this.getTitle()
      .subscribe(function(data:any){
        _this.PostData=data;
        // console.log(data);
      });
  }
  // getUser() {
  //   return this.http.get(this.IDsULR);
  // }
  // UserData={};
  // showUserData() {
  //   var _this=this
  //   this.getUser()
  //     .subscribe(function(data:any){
  //       _this.UserData=data;
  //       // console.log(_this.UserData);
  //     });
  // }
  // IDsULR;
  // ID;
  IfClickedShowUser=0;
  IDsULRParent;
  // goToUserIDClick(user){
  //   this.IfClickedShowUser=1;
  //   // this.ID=user.id;
  //   this.IDsULRParent= 'https://jsonplaceholder.typicode.com/posts/'+ JSON.stringify(user.id);
  //   console.log(this.IDsULRParent);
  //   // this.littleTimmy.showUserData();
  // }
  // showUserData() {
  //   var _this=this
  //   this.getUser()
  //     .subscribe(function(data:any){
  //       _this.UserData=data;
  //       // console.log(_this.UserData);
  //     });
  // }


  ngOnInit() {
    this.showConfig();
  }

}
